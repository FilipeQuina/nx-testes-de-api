import logging
from datetime import datetime

now = datetime.now()

logging.basicConfig(
    filename=f'logs/{str(now.day)}-{str(now.month)}-{str(now.year)}.log',
    level=logging.ERROR, format='%(asctime)s %(levelname)s: %(message)s')


def loggingError(method, assertionText, url, data, header, responseText, responseCode):
    logging.error(
        f'{method}\nAssertion Message: {str(assertionText)}\nURL: {url}\ndata: {data}\nHeaders: {header}\nResponse Code: {responseCode}\nResponse Text: {responseText}')
