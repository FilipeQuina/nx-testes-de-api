import unittest
import requests
import json
from random import randint
import cases

url_server = "smart-api.smartnx.io"
company_id = '74'
token = '5c08166944e1ea59007a8f3ff36c7b9a94a2281dc0fdcfcadbebed5e065675ef'

agent_id = '41200'
contact_id = '14953496'
chat_id = '15785892'


class TestChatbotAgent(unittest.TestCase):

    def test_list_all_agents(self):
        url = f'https://{url_server}/api/v1/chatbot/agents?company_id={company_id}'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
            global agent_id

        except AssertionError as e:
            cases.loggingError("List all Atendence", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError

    def test_list_by_id(self):
        url = f'https://{url_server}/api/v1/chatbot/agents/{agent_id}?company_id={company_id}'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
            self.assertEquals(agent_id, r.json()['id'])
        except AssertionError as e:
            cases.loggingError("List an Atendence", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError

    def test_edit_by_id(self):
        url = f'https://{url_server}/api/v1/chatbot/agents/{agent_id}?company_id={company_id}'
        headers = {
            'token': token
        }
        data = {
            "name": 'Filipe Pacheco',
            "email": 'filipe.pacheco@smartnx.io',
            "login": 'filipe.pacheco@smartnx.io',
            "phone": '553288951404',
            "type": 3
        }
        r = requests.put(url, headers=headers, data=data)
        try:
            self.assertEquals(r.status_code, 204)
        except AssertionError as e:
            cases.loggingError("Edit Agent", e, url,
                               data, headers, r.text, r.status_code)
            raise AssertionError

    def test_remove_by_id(self):
        url = f'https://{url_server}/api/v1/chatbot/agents/{agent_id}A?company_id={company_id}'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.delete(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 400)
        except AssertionError as e:
            cases.loggingError("Remove Agent", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError


class TestChatbotContacts(unittest.TestCase):

    def test_add_contact(self):
        # Add contacts
        url = f'https://{url_server}/api/v1/chatbot/contacts?company_id={company_id}'
        headers = {
            'token': token
        }
        data = {
            "name": "Name Test",
            "email": "contact_email"+str(randint(0, 9999999)).zfill(7)+"@gmail.com",
            "phone": "3299"+str(randint(0, 9999999)).zfill(7)
        }
        r = requests.post(url, headers=headers, data=data)
        try:
            self.assertEquals(r.status_code, 201)
        except AssertionError as e:
            cases.loggingError("Add an Atendence", e, url,
                               data, headers, r.text, r.status_code)
            raise AssertionError

    def test_list_all_contact(self):
        url = f'https://{url_server}/api/v1/chatbot/contacts?company_id={company_id}&page=0'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
            global contact_id
            contact_id = str(r.json()[1]['id'])
        except AssertionError as e:
            cases.loggingError("List all Atendence", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError

    def test_list_contact_by_id(self):
        url = f'https://{url_server}/api/v1/chatbot/contacts/{contact_id}?company_id={company_id}'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("List a Contact", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError

    def test_update_contact(self):
        url = f'https://{url_server}/api/v1/chatbot/contacts/{contact_id}?company_id={company_id}'
        headers = {
            'token': token
        }
        data = {
            "name": "New Name Test",
            "email": "newemail@gmail.com",
            "phone": "3299"+str(randint(0, 9999999)).zfill(7)
        }
        r = requests.put(url, headers=headers, data=data)
        try:
            self.assertEquals(r.status_code, 204)
        except AssertionError as e:
            cases.loggingError("List all Atendence", e, url,
                               data, headers, r.text, r.status_code)
            raise AssertionError

    def test_z_remove_contact(self):
        url = f'https://{url_server}/api/v1/chatbot/contacts/{contact_id}?company_id={company_id}'
        headers = {
            'token': token
        }
        r = requests.delete(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 204)
        except AssertionError as e:
            cases.loggingError("List all Atendence", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError


class TestChatbotAttendance(unittest.TestCase):

    def test_add_attendance(self):
        url = f'https://{url_server}/api/v1/chatbot/chats/email?company_id={company_id}'
        headers = {
            'token': token
        }
        data = {
            "name": "Teste API",
            "phone": "32999999999",
            "email": "teste@testeapi.com",
            "message": "Mensagem teste",
            "department": "24666",
            "subject": "Teste Mensagem",
            "internal": "false"
        }
        r = requests.post(url, headers=headers, data=data)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("List all Atendence", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError
        global chat_id
        chat_id = str(r.json()['id'])

    def test_all_attendance(self):
        url = f'https://{url_server}/api/v1/chatbot/chats?company_id={company_id}&page=1'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)

        except AssertionError as e:
            cases.loggingError("List all Atendence", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError

    def test_an_attendance(self):
        url = f'https://{url_server}/api/v1/chatbot/chats/{chat_id}/messages?company_id={company_id}'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("List an Atendence", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError

    def test_wait_queue(self):
        url = f'https://{url_server}/api/v1/chatbot/chats/{chat_id}/queue?company_id={company_id}'
        headers = {
            'token': token
        }
        r = requests.post(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 201)
        except AssertionError as e:
            cases.loggingError("Wait Queue", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError

    def test_send_message(self):
        url = f'https://{url_server}/api/v1/chatbot/chats/{chat_id}/messages?company_id={company_id}'
        headers = {
            'token': token
        }
        data = {
            "text": "mensagem teste",
            "internal": "true"
        }
        r = requests.post(url, headers=headers, data=data)
        try:
            self.assertEquals(r.status_code, 201)
        except AssertionError as e:
            cases.loggingError("Send Message", e, url,
                               data, headers, r.text, r.status_code)
            raise AssertionError


if __name__ == "__main__":
    unittest.main()
