import unittest
import requests
import json
import cases
from random import randint
import os

url_server = "smart-api.smartnx.io"
company_id = '74'
token = '5c08166944e1ea59007a8f3ff36c7b9a94a2281dc0fdcfcadbebed5e065675ef'
charge_id = ''

class TestCallcenterRecords(unittest.TestCase):

    def test_list_records(self):
        start_date = '2020-02-02'
        end_date = '2020-02-03'
        phone = ''
        url = f'https://{url_server}/api/v1/callcenter/record?company_id={company_id}&start_date={start_date}&end_date={end_date}&phone={phone}'
        headers = {
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("List Records", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError

    def test_record_download(self):
        record_id = '1234'
        url = f'https://{url_server}/api/v1/callcenter/record/{record_id}?company_id={company_id}&date=2020-02-02'
        headers = {
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("Record Download", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError


class TestCallcenterAgents(unittest.TestCase):

    def test_agent_analisys(self):
        start_date = '02/02/2020'
        end_date = '03/02/2020'
        url = f'https://{url_server}/api/v1/callcenter/agents/status?company_id={company_id}&start_date={start_date}&end_date={end_date}'
        headers = {
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("Agent analisys", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError

    def test_pause_agent(self):
        agent_id = '100'
        action = 'pause'
        break_id = '123'
        url = f'https://{url_server}/api/v1/callcenter/agents/{agent_id}/break?company_id={company_id}&action={action}&break_id={break_id}'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.put(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 400)
        except AssertionError as e:
            cases.loggingError("Pause agent", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError


class TestCallcenterCharges(unittest.TestCase):
    
    def test_all_charges(self):
        url = f'https://{url_server}/api/v1/callcenter/charges?company_id={company_id}'
        headers = {
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEqual(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("List all Charges", e, url,
                               "", headers, r.text, r.status_code)
            raise AssertionError
        global charge_id
        charge_id = r.json()["data"]["campanhas"][0]["id_campanha"]

    def test_create_charge(self):

        url = f'https://{url_server}/api/v1/callcenter/charges?company_id={company_id}'
        headers = {
            'token': token
        }
        data = {'layout_id': '15',
                     'active': '0',
                     'mailing_name': 'CargaTeste',
                     'campaign_id': '1'}
        files = [
            ('charge_file', open(os.path.abspath("teste.csv"), 'rb'))
            ]
        r = requests.post(url, headers=headers, data=data, files=files)
        try:
            self.assertEqual(r.status_code, 201)
        except AssertionError as e:
            cases.loggingError("Add Charge", e, url,
                           "", headers, r.text, r.status_code)
            raise AssertionError
        

    def test_merge_charge(self):
        url = f'https://{url_server}/api/v1/callcenter/charges/{charge_id}?company_id={company_id}'
        headers = {
            'token': token
        }
        data = {'layout_id': '15',
                     'active': '0'
                     }
        files = [
            ('charge_file', open(os.path.abspath("teste1.csv"), 'rb'))
            ]
        r = requests.put(url, headers=headers, data=data, files=files)
        try:
            self.assertEqual(r.status_code, 201)
        except AssertionError as e:
            cases.loggingError("Merge Charge", e, url,
                           "", headers, r.text, r.status_code)
            raise AssertionError

    def test_click_to_call(self):
        url = f'https://{url_server}/api/v1/callcenter/calls'
        headers = {
            'X-Api-Token': token
        }
        data = {
            "campaign_id":"1",
            "name":"API-Name",
            "phone_1": "3299"+str(randint(0, 9999999)).zfill(7),
            "prefix_1": "32",
            "phone_type_1":"1"
            }
        r = requests.post(url, headers=headers, data=data)
        try:
            self.assertEqual(r.status_code, 201)
        except AssertionError as e:
            cases.loggingError("Click To Call", e, url,
                           "", headers, r.text, r.status_code)
            raise AssertionError



if __name__ == "__main__":
    unittest.main()