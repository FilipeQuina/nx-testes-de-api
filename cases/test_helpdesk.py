import unittest
import requests
import json
from random import randint
import cases

url_server = "smart-api.smartnx.io"
company_id = '74'
token = '5c08166944e1ea59007a8f3ff36c7b9a94a2281dc0fdcfcadbebed5e065675ef'

agent_id = ''
ticket_id = ''
contact_id = '14339704'
client_id = '14339704'
chat_id = '15785892'
client_doc = '10126133646'


class TestHelpdeskClient(unittest.TestCase):

    def test_1list_all_clients(self):
        url = f'https://{url_server}/api/v1/helpdesk/clients?company_id={company_id}&rows=10&page=1'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("List all Clients", e, url,
                               "-", headers, r.text, r.status_code)
            raise AssertionError
        global client_id
        client_id = str(r.json()['data'][0]['id'])

    def test_2add_client(self):
        url = f'https://{url_server}/api/v1/helpdesk/clients?company_id={company_id}'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        data = {
            "isActive": "true",
            "personType": 1,
            "profileType": 2,
            "businessName": "Smart NX test",
            "corporateName": "Smart NX ME",
            "userName": "Filipenxteste"+str(randint(0, 9999999)).zfill(7),
            "password": "null",
            "cpfCnpj": "10126133646",
            "accessProfile": "Clientes",
            "classification": "null",
            "role": "null",
            "emails": [
                {
                    "emailType": "Profissional",
                    "email": "atendimento@smartnx.com",
                    "isDefault": "true"
                }
            ],
            "contacts": [
                {
                    "contactType": "Telefone comercial",
                    "contact": "(32) 3035-4150",
                    "isDefault": "true"
                },
                {
                    "contactType": "Telefone celular",
                    "contact": "(47) 9999-9999",
                    "isDefault": "false"
                }
            ],
            "addresses": [
                {
                    "postalCode": "89035200",
                    "country": "Brasil",
                    "state": "Minas Gerais",
                    "addressType": "Comercial",
                    "district": "Centro",
                    "street": "Rua Espírito Santo",
                    "city": "Juiz de fora",
                    "number": "1282",
                    "complement": "Sala 201",
                    "reference": "Próximo a Colégio Pio XII",
                    "isDefault": "true"
                }
            ],
            "teams": [
                "Administradores",
                "Consultoria"
            ],
            "relationships": [
                {
                    "id": "523072478",
                    "name": "Smart NX",
                    "slaAgreement": "SLA SmartNX",
                    "services": [
                        {
                            "id": 154664,
                            "name": "Trabalhe conosco"
                        }
                    ]
                }
            ],
            "cultureId": "pt-BR",
            "timeZoneId": "America/Sao_Paulo",
            "createdDate": "2019-08-17T18:00:43.3339728",
            "observations": "Cadastro realizado via api de pessoas."
        }
        r = requests.post(url, headers=headers, json=data)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("Add a Client", e, url,
                               data, headers, r.text, r.status_code)
            raise AssertionError
        global client_id
        client_id = r.json()['data']['id']

    def test_3list_client(self):
        url = f'https://{url_server}/api/v1/helpdesk/clients/5694bd97-ffa6-42df-?company_id={company_id}'
        headers = {
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("Edit Client", e, url,
                               "-", headers, r.text, r.status_code)
            raise AssertionError

    def test_4edit_client(self):
        url = f'https://{url_server}/api/v1/helpdesk/clients/5694bd97-ffa6-42df-?company_id={company_id}'
        headers = {
            'token': token
        }
        data = {
            "name": 'André Barros Santos',
            "email": 'andre.santos@smartnx.com',
            "login": 'andre.santos@smartnx.com',
            "phone": '5532999910050',
            "type": 3
        }
        r = requests.put(url, headers=headers, json=data)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("Add an Attendence", e, url,
                               data, headers, r.text, r.status_code)
            raise AssertionError


class TestChatbotTickets(unittest.TestCase):

    def test_create_ticket(self):
        url = f'https://{url_server}/api/v1/helpdesk/tickets?company_id={company_id}&client_doc={client_doc}'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        data = {
            "type": '1',
            "createdBy": {
                "id": "598099790",
                "personType": '1',
                "profileType": '2'
            },
            "actions": [
                {
                    "type": '1',
                    "origin": '9',
                    "description": u"descrição teste"
                }
            ]
        }
        r = requests.post(url, headers=headers, json=data)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("Create Ticket", e, url,
                               data, headers, r.text, r.status_code)
            raise AssertionError
        

    def test_1list_all_tickets(self):
        url = f'http://{url_server}/api/v1/helpdesk/tickets?company_id={company_id}&rows=10&page=1'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("List all Tickets", e, url,
                               "-", headers, r.text, r.status_code)
            raise AssertionError
        global ticket_id
        ticket_id = str(r.json()['data'][0]['id'])

    def test_list_ticket(self):
        url = f'https://{url_server}/api/v1/helpdesk/tickets/{ticket_id}?company_id={company_id}'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("List an ticket", e, url,
                               "-", headers, r.text, r.status_code)
            raise AssertionError
        

    def test_list_tickets_by_client(self):
        url = f'https://{url_server}/api/v1/helpdesk/tickets/client/{client_id}?company_id={company_id}'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("List Tickets by Client", e, url,
                               "-", headers, r.text, r.status_code)
            raise AssertionError

class test_helpdesk_service(unittest.TestCase):

    def test_list_all_services(self):
        url = f'https://{url_server}/api/v1/helpdesk/services?company_id={company_id}&rows=10&page=1'
        headers = {
            'content-type': 'application/json',
            'token': token
        }
        r = requests.get(url, headers=headers)
        try:
            self.assertEquals(r.status_code, 200)
        except AssertionError as e:
            cases.loggingError("List all services", e, url,
                               "-", headers, r.text, r.status_code)
            raise AssertionError
    

if __name__ == "__main__":
    unittest.main()
