# Projeto de Atumatização de APIs

## Objetivos:
- Validar as APIs de forma sistemática
- Ser uma premissa básica para o proseguimento no deploy

## Pré requisistos
1. Python 3 instalado
2. PIP instalado

## Instruções de instalação do ambiente:
Executar os comandos
~~~
git clone https://FilipeQuina@bitbucket.org/FilipeQuina/nx-testes-de-api.git
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
~~~

## Configurando VS Code para os testes
1. Instalar a seguinte extensão: Python Test Explorer for Visual Studio Code
2. Após a instalação irá aparece no canto esquerdo o seguinte ícone ![](http://i.imgur.com/E8ChXh6.png). Clique neste ícone.
3. Após clickar digite o seguinte comando Ctrl+Alt+P e clique na opção: "Python: Configure tests"
![](http://i.imgur.com/XCTXRG6.png)
4. Configure de acordo as seguintes imagens:
![](http://i.imgur.com/oouFB7V.png)
![](http://i.imgur.com/xHCHvJp.png)
![](http://i.imgur.com/kywtmdN.png)

Resumidamente nós configuramos o **Unittest** que é o nosso framework de teste, as configurações que fizemos foram a pasta que os testes ficarão e qual é o padrão do nome dos arquivos de teste. para que o Unittest consiga captar todos os scripts de teste de nosso projeto.

5. Agora todos os testes ficarão no ícone citado anteriormente conforme print:
![](http://i.imgur.com/xlCRITL.png)
6. Ao passar o mouse, a extensão nos dá duas opções: rodar o script ou debugá-lo.
7. A extensão nos disponibiliza os testes individuais (que são as funções dentro da classe) caso você execute somente este teste, os demais testes da mesma classe não serão executados, caso execute a classe todos os métodos da classe serão executados.

## A estrutura básica para o script de teste
~~~py
# Imports
import unittest
import requests

# Nome da classe deve sempre iniciar com "Test" e deve-se sempre herdar a TestCase do Unittest
class TestHelpdesk(unittest.TestCase):
    
    # setup são os procedimentos que deve ser executados antes dos testes serem executados
    def setUp(self):
        pass
    # Nome da função é o nome do teste e apra o Unittest localizar est métodos, o mesmo deve iniciar com "test_", caso não possua esta nomenclatura o Unittest irá considerá-lo uma função normal e não vai o executar
    def test_inicial_200(self):
        url = 'https://teste.com/test'
        headers = {
            'content-type': 'application/json',
            'token':'123abc
            }
        r = requests.get(url, headers=headers)
        # Os Asserts são o fator que determinar se um teste teve sucesso ou falha, no exemploa abaixo e se varificado se o statuis code é iqual a 200.
        self.assertAlmostEquals(r.status_code,200)

    # O tearDown é o método que é executato no final de cada teste.        
    def tearDown(self):
        pass

# Este bloco de código é necssário para orquestar as execuções dos scripts de teste.
if __name__ == "__main__":
    unittest.main()

~~~




